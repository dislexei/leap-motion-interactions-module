﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandUI : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Debug.Log("Drawing debug line from hand");
    }
	
	// Update is called once per frame
	void Update () {
        Debug.DrawRay(transform.position, -transform.up, Color.green);
    }
}
