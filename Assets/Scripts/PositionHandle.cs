﻿using Leap.Unity.Interaction;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionHandle : Handle {

    public enum TransformAxis { X, Y, Z }
    TransformTool tool;

    // Use this for initialization
    void Start () {
        base.Start();
        tool = gameObject.GetComponentInParent<TransformTool>();

        interaction.OnGraspedMovement += OnGraspedMovementHandler;
    }
	
	// Update is called once per frame
	void Update () {
		
	}


    /* This method uses (handles) the built in Interaction Behaviour event (action) OnGraspedMovement.
     * The event allows us to retrieve the position and rotation information about the grasped object before and after the grasp.
     * 
     * 
    */
    void OnGraspedMovementHandler(Vector3 initialPos, Quaternion initialRot, Vector3 finalPos, Quaternion finalRot, List<InteractionController> controllers)
    {
        // Calculate the constrained movement of the handle along its forward axis only.
        Vector3 deltaPos = finalPos - initialPos;
        // get what is 'forward'
        Vector3 handleForwardDirection = initialRot * -Vector3.left;
        Vector3 deltaAxisPos = handleForwardDirection * Vector3.Dot(handleForwardDirection, deltaPos);

        tool.NotifyHandleMovement(deltaAxisPos);

        //move the handle back to its initial position relative to parent
        gameObject.GetComponent<Rigidbody>().position = initialPos;
        gameObject.GetComponent<Rigidbody>().rotation = initialRot;
    }

   protected override void OnGraspEndHandler()
    {
        //Debug.Log("Released " + this.gameObject.name);
        tool.snapOnRelease();

    }


    /*    From interaction behaviour: 
     *    
     *    
     *     Called directly after this grasped object's Rigidbody has had its position and rotation set
    /// by its currently grasping controller(s). Subscribe to this callback if you'd like to override
    /// the default behaviour for grasping objects, for example, to constrain the object's position or rotation.
    /// 
    /// Use InteractionBehaviour.Rigidbody.position and InteractionBehaviour.Rigidbody.rotation to set the
    /// object's position and rotation. Merely setting the object's Transform's position and rotation is not
    /// recommended unless you understand the difference.
    /// </summary>
    /// <remarks>
    /// This method is called after any OnGraspBegin or OnGraspEnd callbacks, but before OnGraspStay. It is
    /// also valid to move the Interaction object (via its Rigidbody) in OnGraspStay, although OnGraspStay does
    /// not provide pre- and post-solve data in its callback signature.


     *    public Action<Vector3, Quaternion, Vector3, Quaternion, List<InteractionController>> OnGraspedMovement
      = (preSolvedPos, preSolvedRot, solvedPos, solvedRot, graspingControllers) => { };
     * 
     * 
     * 
     * 
    */

    /* END OF  interaction behaviour:
    


    THIS IS FROM LEAP EXAMPLE

    private void onGraspedMovement(Vector3 presolvePos, Quaternion presolveRot,
                                  Vector3 solvedPos, Quaternion solvedRot,
                                  List<InteractionController> controllers)
    {
        /* 
         * OnGraspedMovement provides the position and rotation of the Interaction object
         * before and after it was moved by its grasping hand. This callback only occurs
         * when one or more hands is grasping the Interaction object. In this case, we
         * don't care about how many or which hands are grasping the object, only where
         * the object is moved.
         * 
         * The Translation Handle uses the pre- and post-solve movement information to
         * calculate how the user is trying to move the object along this handle's forward
         * direction. Then the Translation Handle will simply override the movement caused
         * by the grasping hand and reset itself back to its original position.
         * 
         * The movement calculated by the Handle in this method is reported to the Transform
         * Tool, which accumulates movement caused by all Handles over the course of a frame
         * and then moves the target object and all of its child Handles appropriately at
         * the end of the frame.
         */

    /*
// Calculate the constrained movement of the handle along its forward axis only.
Vector3 deltaPos = solvedPos - presolvePos;
    Vector3 handleForwardDirection = presolveRot * Vector3.forward;
    Vector3 deltaAxisPos = handleForwardDirection * Vector3.Dot(handleForwardDirection, deltaPos);

    // Notify the tool about the calculated movement.
    _tool.NotifyHandleMovement(deltaAxisPos);

    // In this case, the tool itself will accumulate delta positions and delta rotations
    // from all handles, and will then synchronize handles to the appropriate positions and
    // rotations at the end of the frame.

    // Because the Tool will be the one to actually move this Handle, all we have left to do
    // is to undo all of the motion caused by the grasping hand.
    _intObj.rigidbody.position = presolvePos;
    _intObj.rigidbody.rotation = presolveRot;

 */
}

