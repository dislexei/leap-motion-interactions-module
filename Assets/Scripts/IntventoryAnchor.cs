﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntventoryAnchor : MonoBehaviour {
    Vector3 startingPosition;

    // Use this for initialization
    void Start () {
        startingPosition = gameObject.transform.localPosition;
	}
	
    private void OnEnable()
    {
        gameObject.transform.localPosition = startingPosition;
    }
}
