﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emitter : MonoBehaviour {
   //public LineRenderer laserLineSource;
   public ParticleSystem emitter;
   //public int lengthOfLineRenderer;
   private Vector3 initialOrientation;
    private Vector3 initialPosition;
    //LaserManager laserManager;
   
    // Use this for initialization
   protected void Start () {

        initialOrientation = this.transform.forward;
        initialPosition = this.transform.position;


    //transform.hasChanged = false;
    //laserManager = GameObject.FindGameObjectWithTag("LaserManager").GetComponent<LaserManager>();

    //laserLineSource = gameObject.GetComponent<LineRenderer>();
        //lengthOfLineRenderer = 2;
        //laserLineSource.positionCount = lengthOfLineRenderer;
        emitter = gameObject.GetComponentInChildren<ParticleSystem>();
        //laserLineSource.enabled = false;

        if (gameObject.tag == "LaserSource")
        {
            SetLaserPoints();
            StartEmit();
            this.AddToLaserList();
        }
        else
        {
            this.AddToMirrorList();
        }

        
	}
	
	// Update is called once per frame
	void Update () {
        if (this.transform.forward != initialOrientation || this.transform.position != initialPosition)
        {
            initialOrientation = this.transform.forward;
            initialPosition = this.transform.position;
            //transform.hasChanged = false;
            Debug.Log("Emission point has changed");
            this.RestartAllEmissions();
            //if (gameObject.tag == "LaserSource")
            //{
            //    SetLaserPoints();
            //}     
        }
        
    }

    public void StartEmit()
    {
        var em = emitter.emission;
        em.enabled = true;
    }

    public void StopEmit()
    {
        var em = emitter.emission;
        em.enabled = false;
    }

    public void disableLaserLine()
    {
        //laserLineSource.enabled = false;
    }

    public void SetLaserPoints()
    {
        //laserLineSource.enabled = true;
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hitPoint;

        //this.laserLineSource.SetPosition(0, ray.origin);

        if (Physics.Raycast(ray, out hitPoint, 20)){
            //laserLineSource.SetPosition(1, hitPoint.point);
            TriggerReflection(hitPoint);
        }

        else {
            //laserLineSource.SetPosition(1, ray.GetPoint(20));
        }
    }

    public void TriggerReflection(RaycastHit _hitPoint)
    {
        GameObject hitObject = _hitPoint.collider.gameObject;
        Debug.Log("I've hit " + hitObject);
        if (hitObject.tag == "Mirror")
        {
            Debug.Log("I've hit an object with tag mirror!");

            Emitter laserEmitter = hitObject.GetComponentInChildren<Emitter>();

            Debug.Log("Found emmitter component in mirror's child: " + laserEmitter);

            laserEmitter.SetLaserPoints();
            laserEmitter.StartEmit();

            ParticleSystem hitEmitter = hitObject.GetComponentInChildren<ParticleSystem>();
            var em = hitEmitter.emission;
            em.enabled = true;
        }
        if (hitObject.tag == "LaserTarget")
        {
            Debug.Log("Brother, this is a triumph!");
            var light = hitObject.GetComponentInChildren<Light>();
            light.color = Color.green;
        }
    }
}
