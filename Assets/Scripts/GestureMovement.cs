﻿using Leap.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestureMovement : MonoBehaviour {

    public gestureMoveHandler gestureHandler;

    public bool debug;
    public GameObject pointingFinger;
    public GameObject middleFinger;
    public GameObject thumbFinger;
    public GameObject cameraTrack;

    public Action toMoveForward;
    public Action toMoveBackward;

    private EnumEventTable _eventTable;

    // Use this for initialization
    void Start () {
        //debug = true;
	}
	
	// Update is called once per frame
	void Update () {
        if (debug)
        {
            Debug.DrawRay(pointingFinger.transform.position.normalized, pointingFinger.transform.forward, Color.red);
            Debug.DrawRay(middleFinger.transform.position.normalized, middleFinger.transform.forward, Color.blue);
            Debug.DrawRay(thumbFinger.transform.position.normalized, thumbFinger.transform.forward, Color.green);
        }

        if (Vector3.Dot(pointingFinger.transform.forward.normalized, middleFinger.transform.forward.normalized) < -0.8F)
        {
            if (Vector3.Dot(pointingFinger.transform.forward.normalized, cameraTrack.transform.forward.normalized) > 0.7F)
            {
                //Debug.Log("Moving forward");
                Vector3 flatMovement = new Vector3(pointingFinger.transform.forward.x, 0, pointingFinger.transform.forward.z);
                //Debug.Log(flatMovement);
                gestureHandler.MoveCharacter(flatMovement);
            }
        }

        if (Vector3.Dot(thumbFinger.transform.forward.normalized, cameraTrack.transform.forward.normalized) < -0.7F)
        {
            //Debug.Log("Moving backwards");
            Vector3 flatMovement = new Vector3(thumbFinger.transform.forward.x, 0, thumbFinger.transform.forward.z);
            gestureHandler.MoveCharacter(flatMovement);
        }

	}



}
