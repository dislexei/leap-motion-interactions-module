﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FaceCameraScript : MonoBehaviour {

    public GameObject CameraToTrack;
    public GameObject palm;
    //public Transform palmTransform;
    public GameObject palmMenu;
    public bool debugMode = true;
    private bool menuEnabled = false;
    private bool facing = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (CheckFacing()) {
            palmMenu.SetActive(true);
        }
        else
        {
            palmMenu.SetActive(false);
        }


    }

    bool CheckFacing()
    {
        //Debug.Log("Camera transform = " + CameraToTrack.transform.position);
        //Debug.Log("Palm transform = " + Palm.transform.position);

        if (debugMode)
        {
            Debug.DrawRay(CameraToTrack.transform.position.normalized, CameraToTrack.transform.forward, Color.red);
            Debug.DrawRay(palm.transform.position, -palm.transform.up, Color.green);
            Debug.Log(Vector3.Dot(CameraToTrack.transform.position.normalized, palm.transform.up));
        }
        /*
        if (debugMode)
        {
            Debug.DrawRay(CameraToTrack.transform.position.normalized, CameraToTrack.transform.forward, Color.red);
            Debug.DrawRay(palm.transform.position, -palm.transform.up, Color.green);
            Debug.Log(Vector3.Dot(CameraToTrack.transform.position.normalized, palm.transform.up));
        }

         if (Vector3.Dot(CameraToTrack.transform.position.normalized , palm.transform.up) < -0.2F)
        {
            Debug.Log("Facing Camera!");
            return true;
        }

        */


        if (Vector3.Dot(CameraToTrack.transform.forward.normalized, -palm.transform.up) < -0.2F)
        {
            //Debug.Log("Facing Camera!");
            return true;
        }
        else
        {
            //Debug.Log(Vector3.Dot((CameraToTrack.transform.position - PalmTransform.position).normalized, PalmTransform.forward));
            return false;
        }
    }

}
