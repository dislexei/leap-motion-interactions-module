﻿using Leap.Unity.Interaction;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationHandle : Handle
{

    //public enum TransformAxis { X, Y, Z }
    TransformTool tool;

    // Use this for initialization
    void Start()
    {
        base.Start();
        tool = gameObject.GetComponentInParent<TransformTool>();

        interaction.OnGraspedMovement += OnGraspedMovementHandler;
    }

    void OnGraspedMovementHandler(Vector3 initialPos, Quaternion initialRot, Vector3 finalPos, Quaternion finalRot, List<InteractionController> controllers)
    {
        //calculates how far the handle was from the tool in the beginning of interaction
        Vector3 presolveToolToHandle = initialPos - tool.transform.position;
        //calculates which direction the handle is from the tool after interaction
        Vector3 solvedToolToHandleDirection = (finalPos - tool.transform.position).normalized;

        Vector3 constrainedToolToHandle = Vector3.ProjectOnPlane(solvedToolToHandleDirection, (initialRot * Vector3.back)).normalized * presolveToolToHandle.magnitude;
        //translates the handle movement to rotation around the handle
        Quaternion deltaRotation = Quaternion.FromToRotation(presolveToolToHandle, constrainedToolToHandle);

        // Notify the tool about the calculated rotation.
        //Debug.Log(deltaRotation);
        tool.NotifyHandleRotation(deltaRotation);

        // Move the object back to its original position, to be moved correctly later on by the Transform Tool.
        interaction.rigidbody.position = initialPos;
        interaction.rigidbody.rotation = initialRot;
    }

    protected override void OnGraspEndHandler()
    {
        //Debug.Log("Released " + this.gameObject.name);
        tool.RoundAngleOnRelease();

    }
}
