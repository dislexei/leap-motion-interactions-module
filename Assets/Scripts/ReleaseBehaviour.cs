﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Leap.Unity.Interaction;
//using UnityEditor;

public class ReleaseBehaviour : MonoBehaviour {
    
    //parent of this gameobject
    private GameObject parent;
    //AnchorableBehaviour of the parent object (actual interactible cube)
    private AnchorableBehaviour parentBehaviour;
    //interaction behaviour script attached to the parent of this gameobject 
    InteractionBehaviour interaction;
    //prefab that should be created upon release outside of anchor
    public GameObject associatedObject;
    //instance of the prefab that has been created
    private GameObject createdObject;
    //is this object currently attached to an anchor?
    private bool isAttached;
    //serves to trach attached/detached state and to prevent spamming notifications
    private bool isCreated;

    public LayerMask myLayerMask;
    private Vector3 objectSize;
    private GameObject physicalObject;
    private GameObject associatedTable;

    public bool needsOffsetting;

    [Range(0.0F, 1.0F)]
    public float anchorHeightOffset = 0F;

   

    void Start () {
        isCreated = false;
        parent = gameObject.transform.parent.gameObject;
        interaction = gameObject.GetComponent<InteractionBehaviour>();
       // interaction.OnGraspBegin = OnGraspHandler;
        parentBehaviour = (AnchorableBehaviour)gameObject.GetComponent(typeof(AnchorableBehaviour));
        interaction.OnGraspBegin += OnGraspBeginHandler;
        interaction.OnGraspEnd += OnGraspEndHandler;
    }



    

	// Update is called once per frame
	void Update () {
        
        if (parentBehaviour.isAttached)
        {

            Debug.DrawRay(transform.position, Vector3.down, Color.blue);  
        }
    }
        
    void OnGraspBeginHandler()
    {
        
        if (isCreated)
        {
            //sets parent to the instantiated object
            //createdObject.transform.parent = this.gameObject.transform;
            //gameObject.transform.parent = createdObject.transform ;
        }
    }

    void OnGraspEndHandler()
    {
        //Vector3 objectPosition;
        RaycastHit groundHit;
        isAttached = parentBehaviour.isAttached;
        //Debug.Log("Attached = " +   isAttached);

        if (!isAttached){
            // myLayerMask = 1 << 8;
            //TODO: create another inventory item in the inventory

            //Debug.Log("working on layer: " + LayerMask.NameToLayer("WorkSurface"));

            Ray ray = new Ray(transform.position, Vector3.down);

            if (Physics.Raycast(ray, out groundHit, 100.0F, myLayerMask)){
                //Debug.Log("Raycast hit: " + groundHit.collider);
                associatedTable = groundHit.collider.gameObject;
                Vector3 objectPosition = groundHit.point;

                if (!isCreated){
                    createdObject = Instantiate(associatedObject, objectPosition.Snap(), associatedObject.transform.rotation);
                
                    //calls the static Snap() method
                    OffsetPosition(createdObject);
                    isCreated = true;
                }

                //makes created object child of table
                createdObject.transform.parent = associatedTable.transform;
                //makes the handle child of the created object
                gameObject.transform.parent = null;
                gameObject.transform.parent = getPhysicalObject(createdObject).transform;

                repositionAnchor(createdObject);
            }
        } 

        /*
        if (Physics.Raycast(transform.position, Vector3.down, out groundHit, 1 << LayerMask.NameToLayer("WorkSurface")) && !parentBehaviour.isAttached && isCreated)
        {
            objectPosition = groundHit.point;
            createdObject.transform.position = objectPosition;
        }
        */

        else if (isAttached && isCreated)
        {
            gameObject.transform.parent = parent.transform;
            createdObject.SetActive(false);
            isCreated = false;
        }
    }

    private void OffsetPosition(GameObject _object)
    {
        //Debug.Log("Offsetting objet's position for " + _object.name);
        physicalObject = getPhysicalObject(_object);
        objectSize = getPhysicalSize(physicalObject);
        //Debug.Log("Size of the object is: " + objectSize);
        //offsets object position by half of its height
        //Debug.Log(associatedObject.name + "'s position before offset: " + _object.transform.position);
        //Debug.Log(associatedObject.name + "'s size " + objectSize);
        _object.transform.localPosition += new Vector3(0, objectSize.y/2, 0);
        //Debug.Log(associatedObject.name + "'s position after offset: " + _object.transform.position);
    }

    //gets the actual 'physical' object representation attached to this object
    private GameObject getPhysicalObject(GameObject associated)
    {
        if (associated.tag == "PhysicalObject")
        {
            return associated;
        }
        else
        {
            Transform parent = associated.transform;
            foreach (Transform child in parent)
            {
                if (child.gameObject.tag == "PhysicalObject")
                {
                    //Debug.Log("Found a physical child " + child.gameObject.name);
                    return child.gameObject;
                }
            }
            return this.gameObject;
        }
    }

    //gets physical size of the object. used for offsetting object position
    private Vector3 getPhysicalSize(GameObject physical)
    {
        if (physical.GetComponent<BoxCollider>())
        {
            return physical.GetComponent<BoxCollider>().bounds.size;
        }
        return new Vector3();
    }

    private Vector3 getVisibleSize(GameObject visible)
    {
        if (visible.GetComponent<MeshRenderer>())
        {
            return visible.GetComponent<MeshRenderer>().bounds.size;
        }
        Debug.Log("Object " + visible.name + " doesn't have meshrenderer attached" );
        return new Vector3();
    }

    void repositionAnchor(GameObject _object)
    {
        Debug.Log("Offsetting the anchor's position. Before = " + this.transform.position);
        
        //Vector3 anchorPosition = new Vector3(groundPosition.x, objectHeight.y*1.1F, groundPosition.z);
        /*(physicalObject = getPhysicalObject(_object);
        objectSize = getPhysicalSize(physicalObject);
        if (objectSize == Vector3.zero)
        {
            Debug.Log("Object " + physicalObject.name + " doesn't have a collider attached. Getting visible size");
            objectSize = getVisibleSize(physicalObject);
        }*/
        //Debug.Log(associatedObject.name + "'s size " + objectSize);

        //Vector3 anchorPosition = new Vector3(_object.transform.position.x, _object.transform.position.y, _object.transform.position.z);
        //this.transform.position = anchorPosition;
        this.transform.position = _object.transform.position + new Vector3(0, anchorHeightOffset, 0);
        //Debug.Log("Offsetting the anchor's position. After = " + this.transform.position);
    }
}

