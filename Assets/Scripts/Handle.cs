﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Leap.Unity.Interaction;

public class Handle : MonoBehaviour {
    protected InteractionBehaviour interaction;


    // Use this for initialization
    protected void Start () {
       
        interaction = GetComponent<InteractionBehaviour>();
        interaction.OnGraspBegin += OnGraspBeginHandler;
        interaction.OnGraspEnd += OnGraspEndHandler;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnGraspBeginHandler()
    {
        //Debug.Log("Stop touching " + this.gameObject.name + "!!");
    }

   protected virtual void OnGraspEndHandler()
    {
        //Debug.Log("Thanks for stopping touching " + this.gameObject.name + ".");

    }

    public void syncRigidbodyWithTransform()
    {
        interaction.rigidbody.position = this.transform.position;
        interaction.rigidbody.rotation = this.transform.rotation;
    }


}
