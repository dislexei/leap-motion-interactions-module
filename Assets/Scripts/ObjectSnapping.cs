﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ObjectSnapping{

    public static float gridSize = 0.2F;
    public static int angleStep = 5;

    public static Vector3 Snap(this Vector3 prePosition)
    {
        //Debug.Log("Initial coordinates: " + prePosition);

        Vector3 snappedPosition = new Vector3();

        snappedPosition.x = Mathf.Round(prePosition.x * 1/gridSize) * gridSize;
        snappedPosition.y = prePosition.y;
        snappedPosition.z = Mathf.Round(prePosition.z * 1/gridSize) * gridSize;

        
        //Debug.Log("Snapped coordinates: " + snappedPosition);
        return snappedPosition;
    }

    public static Vector3 RoundRotation(this Vector3 preRotation)
    {
        //Debug.Log("Got angles: " + preRotation);
        Vector3 finalAngle = new Vector3();

        finalAngle.x = Mathf.Round(preRotation.x / angleStep) * angleStep;
        finalAngle.y = Mathf.Round(preRotation.y / angleStep) * angleStep;
        finalAngle.z = Mathf.Round(preRotation.z / angleStep) * angleStep;

        //Debug.Log("Offered angles: " + finalAngle);
        return finalAngle;
    }
}
