﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gestureMoveHandler : MonoBehaviour {

    public GestureMovement handToTrack;
    public CharacterController character;
    //subscribe to actions from the right hand
    public float speed;

    // Use this for initialization
    void Start () {
        //handToTrack.toMoveForward += toMoveForwardHandler;
        //handToTrack.toMoveBackward += toMoveBackwardHandler;

    }

    // Update is called once per frame
    void Update () {
		
	}
/*
    void toMoveForwardHandler() {
        Debug.Log("I'm told to move FORWARD");
    }

    void toMoveBackwardHandler() {
        Debug.Log("I'm told to move BACKWARD");
    }
*/
    public void MoveCharacter(Vector3 direction)
    {
        character.Move(direction * Time.deltaTime);
    }
}
