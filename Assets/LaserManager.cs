﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LaserManager{

    static List<Emitter> laserList = new List<Emitter>();
    static List<Emitter> mirrorList = new List<Emitter>();

    public static void AddToMirrorList(this Emitter newEmitter)
    {
        Debug.Log("Laser manager added new mirror: " + newEmitter.name);
        mirrorList.Add(newEmitter);
    }

    public static void AddToLaserList(this Emitter newEmitter)
    {
        Debug.Log("Laser manager added new laser: " + newEmitter.name);
        laserList.Add(newEmitter);
    }

    public static void RestartAllEmissions(this Emitter emitter)
    {
        Debug.Log("Laser manager is restarting all emissions. Method called by: " + emitter.name);

        if(mirrorList.Count != 0)
        foreach(Emitter em in mirrorList)
        {
            em.StopEmit();
            //em.disableLaserLine();
        }
        if (laserList.Count != 0)
            foreach (Emitter las in laserList)
            {
                las.SetLaserPoints();
                las.StartEmit();
            }

    }
}
